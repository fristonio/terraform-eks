output "cluster_endpoint" {
  value       = aws_eks_cluster.eks_cluster.endpoint
  description = "Endpoint for the created EKS cluster."
  sensitive   = true
}

output "kubeconfig_ca" {
  value       = aws_eks_cluster.eks_cluster.certificate_authority[0].data
  description = "Certifacte Authority data for the EKS cluster."
  sensitive   = true
}
