output "ng_id" {
  value       = aws_eks_node_group.eks_ng.id
  description = "ID of the created EKS nodegroup."
}
