data "aws_availability_zones" "available" {}

locals {
  subnets = [
    for cidr_block in cidrsubnets(var.vpc_cidr, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6) : cidrsubnets(cidr_block, 2, 2)
  ]

  all_subnets = sort(flatten(local.subnets))
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.64.0"

  name                 = var.vpc_name
  cidr                 = var.vpc_cidr
  azs                  = data.aws_availability_zones.available.names

  // This is the first block without flattening the the subnets.
  public_subnets       = slice(local.all_subnets, 0, 2)
  private_subnets      = slice(local.all_subnets, 2, length(local.all_subnets))

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = "1"
  }
}
